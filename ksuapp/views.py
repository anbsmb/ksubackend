from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import json
from django.db import connection
from django.core import serializers


def getUserById(request):
    userid = str(request.GET['id'])
    raw_query = '''
    select * from "user" u
    left join "role" r on r.id = u.role_id
    where u.id = %s
    '''
    cursor = connection.cursor()
    cursor.execute(raw_query, [userid])
    data = cursor.fetchone()
    return JsonResponse(json.dumps(data, ensure_ascii=False), safe=False)


def getCompetenceById(request):
    userid = str(request.GET['id'])
    raw_query = '''
    select * from journal j2
    left join "user" u2 on u2.id = j2.user_id
    left join task t on t.id = j2.task_id
    left join task_competence2 tc2 on tc2.task_id = j2.task_id
    left join task_competence3 tc3 on tc3.task_id = j2.task_id
    left join competence2 c5 on c5.id = tc2.competences2_id
    left join competence3 c3 on c3.id = tc3.competences3_id
    left join competence c4 on c4.id = c5.competence_id
    where j2.user_id = %s
    '''
    cursor = connection.cursor()
    cursor.execute(raw_query, [userid])
    data = cursor.fetchall()
    data = [tuple(xi for xi in x if xi is not None) for x in data]
    print(data)
    return JsonResponse(json.dumps(data, ensure_ascii=False), safe=False)


def getCourseById(request):
    courseid = str(request.GET['id'])
    raw_query = '''
    select * from task t
    left join course c on c.id = t.course_id
    where t.id=%s
    '''
    cursor = connection.cursor()
    cursor.execute(raw_query, [courseid])
    data = cursor.fetchone()

    return JsonResponse(json.dumps(data, ensure_ascii=False), safe=False)


def getAllCourse(request):
    raw_query = '''
    select * from task t
    left join course c on c.id = t.course_id
    '''
    cursor = connection.cursor()
    cursor.execute(raw_query)
    data = cursor.fetchall()
    data = [tuple(xi for xi in x if xi is not None) for x in data]
    print(data)
    return JsonResponse(json.dumps(data, ensure_ascii=False), safe=False)
