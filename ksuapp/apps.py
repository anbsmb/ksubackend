from django.apps import AppConfig


class KsuappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ksuapp'
